package com.example.gps_g3.model.data;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumeroTest {

    @Test
    void AtingeMaximoValue() {
        Numero num1 = new Numero(1000000, 1000000, 1000000, '*');
        num1.reCalculaResultado();
        assertEquals("Error-LimitValue", num1.toString());
    }
    @Test
    void AtingeMinValue() {
        Numero num1 = new Numero(-1000000, -1000000, -1000000, '*');
        num1.reCalculaResultado();
        assertEquals("Error-LimitValue", num1.toString());
    }
}