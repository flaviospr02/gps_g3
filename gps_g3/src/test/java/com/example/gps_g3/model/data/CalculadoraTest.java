package com.example.gps_g3.model.data;

import com.example.gps_g3.Main;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CalculadoraTest {
/*
    ArrayList<Numero> calculos;
    ArrayList<Date> data;
    StringBuilder sequenciaFibonacci, convToHex, convToBin;
    String fatorial, volumeCilindro, volumeCone;

    public CalculadoraTest(){
        calculos = new ArrayList<>();
        calculos.add(new Numero(0,' '));
        data = new ArrayList<>();
        sequenciaFibonacci = new StringBuilder();
        convToHex = new StringBuilder();
        convToBin = new StringBuilder();
    }*/

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void invertesinal() {
        Numero num = new Numero(2, '+');
        num.plusminus();
        assertEquals(-2, num.getNumero());
    }

    @Test
    public void soma() {
        Calculadora calculadora = new Calculadora();
        calculadora.seleciona(3);
        calculadora.soma();
        calculadora.seleciona(3);
        assertEquals(" 3.0 + 3.0", calculadora.toString());
    }

    @Test
    public void descalcula(){
        Calculadora calculadora = new Calculadora();
        calculadora.seleciona(3);
        calculadora.soma();
        calculadora.seleciona(4);
        calculadora.multiplicacao();
        calculadora.descalcula();
        assertEquals(" 3.0 + 4.0", calculadora.toString());
    }

    @Test
    public void subtracao() {
    }

    @Test
    public void limpar() {
    }

    @Test
    public void divisao() {
    }

    @Test
    public void multiplicacao() {
    }

    @Test
    public void raiz() {
        Calculadora calculadora = new Calculadora();
        calculadora.seleciona(9);
        calculadora.raiz();
        assertEquals(" 3.0", calculadora.toString());
    }

    @Test
    public void fatorial(){
        Main.calculadoraManager.calcularFatorial("5");
        System.out.println(Main.calculadoraManager.getResultadoFAtorial());
    }
    @Test
    public void verificaFatorialValMaximo(){
        Calculadora calculadora = new Calculadora();
        calculadora.calcularFatorial("100000");
        assertEquals(calculadora.getResultadoFAtorial(), "Error-LimitValue");
    }
    @Test
    public void verificaFatorialDadosInvalidos(){
        Calculadora calculadora = new Calculadora();
        calculadora.calcularFatorial("3r");
        assertEquals(calculadora.getResultadoFAtorial(), "Dados de input inválidos");
    }

    @Test
    public void fibonacciDadosInvalidos(){
        Calculadora calculadora = new Calculadora();
        calculadora.calcularFibonacci("e", "4");
        assertEquals(calculadora.getResultadoFibonacci(), "Dados de input inválidos");
    }

    @Test
    public void bintohex(){
        Main.calculadoraManager.binToHex(01101010);
        System.out.println(Main.calculadoraManager.getHexRes());
    }

    @Test
    public void hextobin(){
        Main.calculadoraManager.hexToBin("8");
        System.out.println(Main.calculadoraManager.getBinRes());
    }

    @Test
    public void volumeCone(){
        Main.calculadoraManager.calculaVolumeCone("5", "8");
        System.out.println(Main.calculadoraManager.getResultadoVolCone());
    }

    @Test
    public void volumeCilindro(){
        Main.calculadoraManager.calculaVolumeCilindro("5", "8");
        System.out.println(Main.calculadoraManager.getResultadoVolCilindro());
    }


}