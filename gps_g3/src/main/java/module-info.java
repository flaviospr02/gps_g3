module com.example.gps_g3 {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires net.synedra.validatorfx;
    requires org.kordamp.ikonli.javafx;
    requires org.kordamp.bootstrapfx.core;
    //requires eu.hansolo.tilesfx;
    requires com.almasb.fxgl.all;

    opens com.example.gps_g3 to javafx.fxml;
    exports com.example.gps_g3;
    exports com.example.gps_g3.ui;
    opens com.example.gps_g3.ui to javafx.fxml;
    exports com.example.gps_g3.ui.stackUIs;
    opens com.example.gps_g3.ui.stackUIs to javafx.fxml;
}