package com.example.gps_g3.model.data;

public class Numero {
    private double resultado, resAnt, numero;
    private char operacao;
    private String mostrarResultado;

    public Numero(double num, double res, double rant, char op){
        numero = num;
        resultado = res;
        resAnt = rant;
        operacao = op;
    }
    public Numero(double num, char op){
        resAnt = num;
        numero = 0;
        operacao = op;
    }

    public double getResultado() {
        return resultado;
    }

    public double potencia() {
        resultado = resAnt;
        for(int  i = 1; i < numero; i++)
            resultado *= resultado;
        return resultado;
    }

    @Override
    public String toString(){
        reCalculaResultado();
        return mostrarResultado;
    }

    public void seleciona(double num) {
        numero *= 10;
        numero += num;
    }

    public void reCalculaResultado() {
        resultado = switch (operacao) {
            case '*' -> resAnt * numero;
            case '-' -> resAnt - numero;
            case '/' -> resAnt / numero;
            case '+' -> resAnt + numero;
            case '^' -> potencia();
            default -> 0;
        };
        if (resultado >= Integer.MAX_VALUE || resultado < Integer.MIN_VALUE)
            mostrarResultado = "Error-LimitValue";
        else
            mostrarResultado = resAnt != 0 ? " " + resAnt + " " + operacao + " " + numero : " " + numero;
    }

    public void plusminus(){
        numero *= -1;
    }

    public double getNumero() {
        return numero;
    }

    public double getAntigo() {
        return resAnt;
    }

    public void limpa() {
        resultado = 0;
        resAnt = 0;
        numero = 0;
        operacao = ' ';
    }

    public double getResAntigo() {
        return resAnt;
    }

    public char getOperacao() {
        return operacao;
    }
}