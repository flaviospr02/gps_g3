package com.example.gps_g3.model;

import com.example.gps_g3.model.data.Calculadora;

import java.time.LocalDate;
import java.util.Date;

public class CalculadoraManager {

    Calculadora calc;

    public CalculadoraManager(){
        calc = new Calculadora();
    }

    public void potencia(){
        calc.potencia();
    }

    public void soma(){
        calc.soma();
    }

    public void subtracao(){
        calc.subtracao();
    }
    public void seleciona(double num){
        calc.seleciona(num);
    }
    public void descalcula(){
        calc.descalcula();
    }
    public void fimLinhaCalculo(){
        calc.fimLinha();
    }
    @Override
    public String toString(){
        return calc.toString();
    }

    public void limpar() {
        calc.limpar();
    }

    public void divisao() {
        calc.divisao();
    }

    public void multiplicacao() {
        calc.multiplicacao();
    }

    public void invsinal() {calc.invertesinal();}

    public void raizquad() {calc.raiz();}

    //Data -----------------------------------------------------------------------------------
    public void diferencaDaData(LocalDate i, LocalDate o) {
        calc.converteLocalDatetoDate(i,o);
    }

    public void diferencaDaData(Date i, Date o) {
        calc.diferencaDaData(i,o);
    }

    public String toStringDiferenceForDates(String s) {
        return calc.toStringDiferenceForDates(s);
    }

    public String DateDia(String s, LocalDate i, boolean adicionaDia){
        return calc.toStringDateAddDay(s,i,adicionaDia);
    }
    //Fibonacci ------------------------------------------------------------------------------
    public void calcularFibonacci(String nInferior, String nSuperior) {
        calc.calcularFibonacci(nInferior, nSuperior);
    }

    public String getResultadoFibonacci() {
        return calc.getResultadoFibonacci();
    }
    //Fatorial
    /*
    public double calcularFatorial(double numeroValue) {
        return calc.calcularFatorial(numeroValue);
    }*/
    public String getResultadoFAtorial() {
        return calc.getResultadoFAtorial();
    }
    public void calcularFatorial(String numeroValue) {
        calc.calcularFatorial(numeroValue);
    }

    //Conversões ------------------------------------------------------------------------------
    public void binToHex(long val){calc.convBinToHex(val);}

    public void hexToBin(String val){calc.convHexToBin(val);}

    public String getHexRes(){return calc.getHexVal();}

    public String getBinRes(){return calc.getBinVal();}

    //Cilindro/Cone--------------------------------------------------------------------------------
    //public double getVolumeCilindro(double raio,double altura){return calc.getVolumeCilindro(raio,altura);}
    public void calculaVolumeCilindro(String raio,String altura){ calc.calculaVolumeCilindro(raio,altura);}
    public void calculaVolumeCone(String raio,String altura){ calc.calculaVolumeCone(raio,altura);}

    public String getResultadoVolCilindro() {
        return calc.getResultadoCilindro();
    }
    public String getResultadoVolCone() {
        return calc.getResultadoCone();
    }

    //public double getVolumeCone(double raio,double altura){return calc.getVolumeCone(raio,altura);}

}