package com.example.gps_g3.model.data;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

public class Calculadora {
    ArrayList<Numero> calculos;
    ArrayList<Date> data;
    StringBuilder sequenciaFibonacci, convToHex, convToBin;
    String fatorial, volumeCilindro, volumeCone;

    public Calculadora(){
        calculos = new ArrayList<>();
        calculos.add(new Numero(0,' '));
        data = new ArrayList<>();
        sequenciaFibonacci = new StringBuilder();
        convToHex = new StringBuilder();
        convToBin = new StringBuilder();
    }
    public void descalcula(){
        if(calculos.size() > 1)
            calculos.remove(calculos.size() - 1);
        else
            calculos.get(0).limpa();
    }
    public void fimLinha() {
        Numero aux = new Numero(calculos.get(calculos.size() - 1).getResultado(), 0, 0, ' ');
        calculos.clear();
        calculos.add(aux);
    }

    @Override
    public String toString(){
        return calculos.get(calculos.size() - 1).toString();
    }

    public void seleciona(double num) {
        calculos.get(calculos.size() - 1).seleciona(num);
    }

    public void invertesinal(){
        calculos.get(calculos.size() - 1).plusminus();
    }

    public void soma() {
        Numero aux = new Numero( calculos.get(calculos.size() - 1).getResultado() == 0 && calculos.get(calculos.size() - 1).getAntigo() == 0 ? calculos.get(calculos.size() - 1).getNumero() : calculos.get(calculos.size() - 1).getResultado(), '+');
        calculos.add(aux);
    }

    public void potencia() {
        Numero aux = new Numero( calculos.get(calculos.size() - 1).getResultado() == 0 ?
                calculos.get(calculos.size() - 1).getNumero() : calculos.get(calculos.size() - 1).getResultado(), '^');
        calculos.add(aux);
    }

    public void subtracao() {
        Numero aux;
        if(calculos.get(calculos.size() - 1).getAntigo() == 0)
            aux = new Numero( calculos.get(calculos.size() - 1).getNumero(), '-');
        else
            aux = new Numero( calculos.get(calculos.size() - 1).getResultado(), '-');
        calculos.add(aux);
    }

    public void limpar() {
        calculos.clear();
        calculos.add(new Numero(0, ' '));
    }

    public void divisao() {
        Numero aux = new Numero( calculos.get(calculos.size() - 1).getResultado() == 0 &&
                calculos.get(calculos.size() - 1).getAntigo() == 0 ? calculos.get(calculos.size() - 1).getNumero() : calculos.get(calculos.size() - 1).getResultado(), '/');
        calculos.add(aux);
    }
    public void multiplicacao() {
        Numero aux = new Numero( calculos.get(calculos.size() - 1).getResultado() == 0 &&
                calculos.get(calculos.size() - 1).getAntigo() == 0 ? calculos.get(calculos.size() - 1).getNumero() : calculos.get(calculos.size() - 1).getResultado(), '*');
        calculos.add(aux);
    }

    public void raiz() {
        Numero aux = new Numero(Math.sqrt(calculos.get(calculos.size() - 1).getNumero()), calculos.get(calculos.size() - 1).getResultado(),
                calculos.get(calculos.size() - 1).getResAntigo(), calculos.get(calculos.size() - 1).getOperacao());
        calculos.add(aux);
    }

    //Data -------------------------------------------------------------------------------------------------
    public void converteLocalDatetoDate(LocalDate din, LocalDate dout) {
        Date indate = retornaDateFormat(din);
        Date outdate = retornaDateFormat(dout);
        diferencaDaData(indate,outdate);
    }
    public void diferencaDaData(Date in, Date out) {
        data.clear();
        data.add(in);
        data.add(out);
    }
    public String toStringDiferenceForDates(String s) {
        if (data.size() < 2)
            return null;
        long differenceTime = data.get(1).getTime() - data.get(0).getTime();
        /*long differenceSecond = (differenceTime / 1000) % 60;
        long differenceMinute = (differenceTime / (1000 * 60)) % 60;
        long differenceHour = (differenceTime / (1000 * 60 * 60)) % 24;*/
        if (s.equalsIgnoreCase("D")) {
            long differenceDay = (differenceTime / (1000 * 60 * 60 * 24)) % 365;
            return Long.toString(differenceDay);
        } else if (s.equalsIgnoreCase("Y")) {
            long differenceYear = (differenceTime / (1000L * 60 * 60 * 24 * 365));
            return Long.toString(differenceYear);
        } else
            return null;
    }
    public String toStringDateAddDay(String s, LocalDate i, boolean d) {
        return retornaDateFormat(Dia(s,i,d)).toString();
    }
    private LocalDate Dia(String s, LocalDate din, boolean adiciona) {
        LocalDate d;
        try {
            int aux = Integer.parseInt(s);
            if (adiciona)
                d = din.plusDays(aux);
            else
                d = din.minusDays(aux);
        } catch (Exception e) {
            return din;
        }
        return d;
    }
    private Date retornaDateFormat(LocalDate d) {
        ZoneId defaultZoneId = ZoneId.systemDefault();
        LocalDate localDatein = LocalDate.of(d.getYear(), d.getMonth(), d.getDayOfMonth());
        return Date.from(localDatein.atStartOfDay(defaultZoneId).toInstant());
    }
    public void calcularFatorial(String numero){

        try {
            double numeroValue = Double.parseDouble(numero);
            double resFatorial = numeroValue;

            for(int i = 1; i < numeroValue; i++)
                resFatorial *= i;

            if (resFatorial >= Integer.MAX_VALUE || resFatorial < Integer.MIN_VALUE)
                fatorial = "Error-LimitValue";
            else
                fatorial = "Fatorial: " + resFatorial;

        }catch (NumberFormatException e) {
            fatorial = "Dados de input inválidos";
        }
    }
    public String getResultadoFAtorial() {
        return fatorial;
    }


    public void calcularFibonacci(String nInferior, String nSuperior) {
        int lim1, lim2;
        sequenciaFibonacci.setLength(0);
        try {
            lim1 = Integer.parseInt(nInferior);
            lim2 = Integer.parseInt(nSuperior);

            double sqrt5 = Math.sqrt(5);
            double f1 = (Math.pow((1 + Math.sqrt(5)), lim1) - Math.pow((1 - Math.sqrt(5)), lim1)) / (Math.pow(2, lim1) * sqrt5);
            double f2 = (Math.pow((1 + Math.sqrt(5)), lim1 + 1) - Math.pow((1 - Math.sqrt(5)), lim1 + 1)) / (Math.pow(2, lim1 + 1) * sqrt5);
            double faux;
            sequenciaFibonacci.append(" ").append((int)f1).append(", ").append((int)f2);
            for(double i = lim1 + 2; i <= lim2; i++){
                faux = f2;
                f2 += f1;
                f1 = faux;
                sequenciaFibonacci.append(", ").append((int)f2);
            }
        }
        catch (NumberFormatException e){
            sequenciaFibonacci.append("Dados de input inválidos");
        }
    }

    public String getResultadoFibonacci() {
        return sequenciaFibonacci.toString();
    }

    //converte ----------------------------------------------------
    public void convBinToHex(long binNum){

        convToHex.setLength(0);

        int decNum = binToDec(binNum);
        char arr[] = { 'A', 'B', 'C', 'D', 'E', 'F' };
        int remainder, i = 0;
        String hexNumber = "";

        while (decNum != 0) {
            remainder = decNum % 16;
            if (remainder >= 10)
                hexNumber = arr[remainder - 10] + hexNumber;
            else
                hexNumber = remainder + hexNumber;
            decNum /= 16;
        }
        convToHex.append(hexNumber);
    }

    public void convHexToBin(String hexNum){
        convToBin.setLength(0);

        char ch;
        String binRes = "";
        int returnBin = 0, i;
        hexNum = hexNum.toUpperCase();

        for (i = 0; i < hexNum.length(); i++) {
            ch = hexNum.charAt(i);
            if (!Character.isDigit(ch) && !((int) ch >= 65 && (int) ch <= 70)) {
                convToBin.append("Valor Hexadecimal invalido!");
                binRes = "";
                break;
            }
            else if ((int)ch >= 65 && (int)ch <= 70)
                returnBin = (int)ch - 55;
            else
                returnBin
                        = Integer.parseInt(String.valueOf(ch));
            binRes += decimalToBinary(returnBin);
        }
        convToBin.append(binRes);
    }

    public int binToDec(long binNum){
        int decNum = 0, i = 0;
        while (binNum > 0){
            decNum += (int) (Math.pow(2, i++) * (binNum % 10));
            binNum /= 10;
        }
        return decNum;
    }

    public String decimalToBinary(long dec){
        String binaryString = "";

        while (dec != 0) {
            binaryString = (dec % 2) + binaryString;

            dec /= 2;
        }
        while (binaryString.length() % 4 != 0) {
            binaryString = "0" + binaryString;
        }
        return binaryString;
    }

    public String getHexVal(){
        return convToHex.toString();
    }

    public String getBinVal(){
        return convToBin.toString();
    }
    /*public double getVolumeCilindro(double raioValue,double alturaValue){
        return Math.PI * Math.pow(raioValue, 2) * alturaValue;
    }*/
    public void calculaVolumeCilindro(String raio, String altura){
        try {
            double raioValue = Double.parseDouble(raio);
            double alturaValue = Double.parseDouble(altura);

            double vol = Math.PI * Math.pow(raioValue, 2) * alturaValue;
            if (vol >= Integer.MAX_VALUE || vol < Integer.MIN_VALUE)
                volumeCilindro = "Error-LimitValue";
            else
                volumeCilindro = "Volume do Cilindro: " + vol  + " u.v.";

        } catch (NumberFormatException ex) {
            volumeCilindro = "Por favor, insira números válidos para raio e altura.";
        }
    }
    public void calculaVolumeCone(String raio, String altura) {
        try {
            double raioValue = Double.parseDouble(raio);
            double alturaValue = Double.parseDouble(altura);

            double vol = (1.0 / 3.0) * Math.PI * Math.pow(raioValue, 2) * alturaValue;

            if (vol >= Integer.MAX_VALUE || vol < Integer.MIN_VALUE)
                volumeCone = "Error-LimitValue";
            else
                volumeCone = "Volume do Cone: " + vol + " u.v.";

        } catch (NumberFormatException ex) {
            volumeCone = "Por favor, insira números válidos para raio e altura.";
        }
    }
    public String getResultadoCilindro() {
        return volumeCilindro;
    }

    public String getResultadoCone() {
        return volumeCone;
    }

}