package com.example.gps_g3.ui;

import com.example.gps_g3.Main;
import com.example.gps_g3.ui.stackUIs.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;

public class MyCalcRootPane extends VBox {
    StackPane stackPane;
    CalculadoraUI calculadoraUI;
    VolumeConeUI calcConeUI;
    CalcDatasUI calcDatasUi;
    ConversoesUI conversoesUI;
    FatorialUI fatorialUI;
    FibonacciUI fibonacciUI;
    VolumeCilindroUI volumeCilindroUI;
    MenuItem calculadora, volumeCone, volumeCilindro, datas, conversor, fatorial, fibonacci;

    public MyCalcRootPane() {
        createViews();
        registerHandler();
        update();
    }

    private void createViews() {
        calculadora = new MenuItem("Calculadora");
        volumeCone = new MenuItem("Volume Cone");
        volumeCilindro = new MenuItem("Volume Cilindro");
        datas = new MenuItem("Datas");
        conversor = new MenuItem("Conversor");
        fatorial = new MenuItem("Fatorial");
        fibonacci = new MenuItem("Fibonacci");

        Menu menu = new Menu("Menu");
        menu.getItems().addAll(calculadora, datas, volumeCone, volumeCilindro, conversor, fatorial, fibonacci);

        MenuBar menuBar = new MenuBar(menu);

        calculadoraUI = new CalculadoraUI();
        calcDatasUi = new CalcDatasUI();
        calcConeUI = new VolumeConeUI();
        conversoesUI = new ConversoesUI();
        fatorialUI = new FatorialUI();
        fibonacciUI = new FibonacciUI();
        volumeCilindroUI = new VolumeCilindroUI();

        calculadoraUI.setVisible(true);
        calcDatasUi.setVisible(false);
        calcConeUI.setVisible(false);
        conversoesUI.setVisible(false);
        fatorialUI.setVisible(false);
        fibonacciUI.setVisible(false);
        volumeCilindroUI.setVisible(false);

        stackPane = new StackPane(calculadoraUI, calcDatasUi, calcConeUI,volumeCilindroUI, conversoesUI, fatorialUI, fibonacciUI);

        this.getChildren().addAll(menuBar, stackPane);
    }

    private void registerHandler() {
        calculadora.setOnAction( actionEvent -> {
            Main.opcao.set(1);
        });
        datas.setOnAction( actionEvent -> {
            Main.opcao.set(2);
        });
        conversor.setOnAction( actionEvent -> {
            Main.opcao.set(3);
        });
        volumeCone.setOnAction( actionEvent -> {
            Main.opcao.set(4);
        });
        volumeCilindro.setOnAction( actionEvent -> {
            Main.opcao.set(7);
        });
        fatorial.setOnAction( actionEvent -> {
            Main.opcao.set(5);
        });
        fibonacci.setOnAction( actionEvent -> {
            Main.opcao.set(6);
        });
    }
/*
    private void newScene(){
        final Stage d = new Stage();
        d.initModality(Modality.APPLICATION_MODAL);
        d.initOwner(this.getScene().getWindow());
        d.setTitle("Avançado");
        VBox vbox = new VBox(20);
        botaoData = new Button("Cálculo de Datas");
        botaoData.setAlignment(Pos.CENTER);
        botaoCalcC = new Button("Volume do Cone");
        botaoCalcC.setAlignment(Pos.CENTER);
        botaoCalcCl = new Button("Volume do Cilindro");
        botaoCalcCl.setAlignment(Pos.CENTER);
        botaoFatorial = new Button("Fatorial");
        botaoFatorial.setAlignment(Pos.CENTER);

        botaoData.setFont(Font.font(17));
        botaoData.setPrefSize(170,50);
        botaoCalcC.setFont(Font.font(17));
        botaoCalcC.setPrefSize(170,50);
        botaoCalcCl.setFont(Font.font(17));
        botaoCalcCl.setPrefSize(170,50);
        botaoFatorial.setFont(Font.font(17));
        botaoFatorial.setPrefSize(170,50);

        vbox.setPadding(new Insets(10));
        vbox.setAlignment(Pos.CENTER);
        vbox.getChildren().addAll(botaoData,botaoCalcC,botaoCalcCl,botaoFatorial);
        Scene Avancado = new Scene(vbox, 270, 350);
        d.setScene(Avancado);
        d.show();
        //--------------------
        botaoData.setOnAction(e -> DataScene());
        botaoCalcC.setOnAction(e-> ConeScene());
        botaoCalcCl.setOnAction(e-> CilindroScene());
        botaoFatorial.setOnAction(e-> FatorialScene());
    }
    private void ConeScene(){
        final Stage d = new Stage();
        d.initModality(Modality.APPLICATION_MODAL);
        d.initOwner(this.getScene().getWindow());
        VBox vbox = new VBox(20);
        d.setTitle("Volume Cone");
        TextField raio = new TextField("Raio");
        TextField altura = new TextField("Altura");
        Button Calcula = new Button("Calcular");
        Label resultadoLabel = new Label();
        vbox.getChildren().addAll(raio,altura,Calcula,resultadoLabel);

        Scene cone = new Scene(vbox, 300, 350);
        d.setScene(cone);
        d.show();
        Calcula.setOnAction(e->{
            try {
                double raioValue = Double.parseDouble(raio.getText());
                double alturaValue = Double.parseDouble(altura.getText());

                // Fórmula do volume do cone: V = (1/3) * π * raio^2 * altura
                double volume = (1.0 / 3.0) * Math.PI * Math.pow(raioValue, 2) * alturaValue;

                resultadoLabel.setText("Volume do Cone: " + volume);
            } catch (NumberFormatException ex) {
                resultadoLabel.setText("Por favor, insira números válidos para raio e altura.");
            }
        });
    }
    private void CilindroScene(){
        final Stage d = new Stage();
        d.initModality(Modality.APPLICATION_MODAL);
        d.initOwner(this.getScene().getWindow());
        VBox vbox = new VBox(20);
        d.setTitle("Volume Cilindro");
        TextField raio = new TextField("Raio");
        TextField altura = new TextField("Altura");
        Button Calcula = new Button("Calcular");
        Label resultadoLabel = new Label();
        vbox.getChildren().addAll(raio,altura,Calcula,resultadoLabel);

        Scene cilindro = new Scene(vbox, 300, 350);
        d.setScene(cilindro);
        d.show();
        Calcula.setOnAction(e->{
            try {
                double raioValue = Double.parseDouble(raio.getText());
                double alturaValue = Double.parseDouble(altura.getText());

                // Fórmula do volume do cone: V = (1/3) * π * raio^2 * altura
                double volume = Math.PI * Math.pow(raioValue, 2) * alturaValue;

                resultadoLabel.setText("Volume do Cilindro: " + volume);
            } catch (NumberFormatException ex) {
                resultadoLabel.setText("Por favor, insira números válidos para raio e altura.");
            }
        });
    }
    private void FatorialScene(){
        final Stage d = new Stage();
        d.initModality(Modality.APPLICATION_MODAL);
        d.initOwner(this.getScene().getWindow());
        VBox vbox = new VBox(20);
        d.setTitle("Fatorial");
        TextField numero = new TextField("Numero");
        Button Calcula = new Button("Calcular");
        Label resultadoLabel = new Label();
        vbox.getChildren().addAll(numero,Calcula,resultadoLabel);

        Scene fatorial = new Scene(vbox, 300, 350);
        d.setScene(fatorial);
        d.show();
        Calcula.setOnAction(e->{
            try {
                double numeroValue = Double.parseDouble(numero.getText());

                double resFatorial = numeroValue;

                for(int i = 1; i < numeroValue; i++)
                    resFatorial *= i;

                resultadoLabel.setText("Fatorial: " + resFatorial);
            } catch (NumberFormatException ex) {
                resultadoLabel.setText("Por favor, insira um número válido para calcular o fatorial.");
            }
        });
    }
<<<<<<< HEAD
=======
    private void DataScene() {
        final Stage d = new Stage();
        d.initModality(Modality.APPLICATION_MODAL);
        d.initOwner(this.getScene().getWindow());
        VBox vbox = new VBox(20);
        d.setTitle("Calcular Datas");

        Button btnAdd = new Button("Adicionar");
        Button btnSub = new Button("Subtrair");
        Button btnDif = new Button("Diferença entre datas");
        Label lbldiferencaData = new Label();
        Label lblInputDia = new Label("Dia:");
        Label lblOutDia = new Label();
        TextField txtNum = new TextField();

        btnAdd.setFont(Font.font(17));
        btnAdd.setPrefSize(150,50);
        btnSub.setFont(Font.font(17));
        btnSub.setPrefSize(150,50);
        btnDif.setFont(Font.font(13));
        btnDif.setPrefSize(150,50);
        lbldiferencaData.setFont(Font.font(17));
        lblInputDia.setFont(Font.font(13));
        lblOutDia.setFont(Font.font(12));
        txtNum.setFont(Font.font(13));
        txtNum.setMaxWidth(50);

        DatePicker InDatePicker = new DatePicker();
        InDatePicker.setValue(LocalDate.now());
        DatePicker OutDatePicker = blockDatePicker(InDatePicker);
        OutDatePicker.setValue(LocalDate.now().plusDays(1));

        GridPane gridPane = new GridPane();
        gridPane.setHgap(5);
        gridPane.setVgap(5);

        Label Inlabel = new Label("De:");
        gridPane.add(Inlabel, 0, 0);
        GridPane.setHalignment(Inlabel, HPos.LEFT);
        gridPane.add(lblInputDia, 1, 0);

        gridPane.add(InDatePicker, 0, 1);
        gridPane.add(txtNum,1,1);

        Label Outlabel = new Label("Até:");
        gridPane.add(Outlabel, 0, 2);
        GridPane.setHalignment(Outlabel, HPos.LEFT);

        gridPane.add(OutDatePicker, 0, 3);
        gridPane.add(lblOutDia,1,3);

        gridPane.addRow(4, btnAdd, btnSub);
        gridPane.addRow(5,btnDif, lbldiferencaData);
        vbox.setPadding(new Insets(10));
        vbox.getChildren().add(gridPane);

        Scene dScene = new Scene(vbox, 410, 250);
        d.setScene(dScene);
        d.show();

        btnDif.setOnAction(e -> {
            Main.calculadoraManager.diferencaDaData(InDatePicker.getValue(),OutDatePicker.getValue());
            String aux = Main.calculadoraManager.toStringDiferenceForDates();
            lbldiferencaData.setText(aux == null ? "Selecione a data" : Integer.parseInt(aux) > 1 ? aux + " dias" : aux + " dia");
        });
        btnAdd.setOnAction(e -> lblOutDia.setText("Data: " +
                Main.calculadoraManager.DateDia(txtNum.getText(),InDatePicker.getValue(),true)));
        btnSub.setOnAction(e -> lblOutDia.setText("Data: " +
                Main.calculadoraManager.DateDia(txtNum.getText(),InDatePicker.getValue(),false)));
    }
>>>>>>> 7e99ae58e55255dde891a9d855b6eb5c26e991d1

    private DatePicker blockDatePicker(DatePicker InDatePicker) {
        DatePicker OutDatePicker = new DatePicker();

        final Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
                    @Override
                    public DateCell call(final DatePicker datePicker) {
                        return new DateCell() {
                            @Override
                            public void updateItem(LocalDate item, boolean empty) {
                                super.updateItem(item, empty);

                                if (item.isBefore(InDatePicker.getValue().plusDays(1))) {
                                    setDisable(true);
                                    setStyle("-fx-background-color: #a4a4a4;");
                                }
                                long d = ChronoUnit.DAYS.between(InDatePicker.getValue(), item);
                                setTooltip(new Tooltip("Diferença de " + d + " dias"));
                            }
                        };
                    }
                };
        OutDatePicker.setDayCellFactory(dayCellFactory);
        return OutDatePicker;
    }
*/
    private void update() {
    }
}
