package com.example.gps_g3.ui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MyCalculator extends Application {
    @Override
    public void init() throws Exception {
        super.init();
    }

    @Override
    public void start(Stage stage) {
        MyCalcRootPane MyCalc = new MyCalcRootPane();
        Scene scene = new Scene(MyCalc,305,380);
        stage.setScene(scene);
        stage.setTitle("MyCalculator");
        stage.setResizable(false);
        stage.show();
    }
}
