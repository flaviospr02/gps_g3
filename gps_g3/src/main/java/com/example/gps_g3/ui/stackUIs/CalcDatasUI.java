package com.example.gps_g3.ui.stackUIs;

import com.example.gps_g3.Main;
import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.util.Callback;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class CalcDatasUI extends BorderPane {
    Button btnAdd, btnSub, btnDif, botaoData;
    Label lbldiferencaData, lblInputDia, lblOutDia;
    TextField txtNum;
    DatePicker InDatePicker, OutDatePicker;
    public CalcDatasUI(){
        createViews();
        registerHandlers();
        update();
    }
    public void createViews(){
        btnAdd = new Button("Adicionar");
        btnSub = new Button("Subtrair");
        btnDif = new Button("Diferença entre datas");
        lbldiferencaData = new Label();
        lblInputDia = new Label("Dia:");
        lblOutDia = new Label();
        txtNum = new TextField();
        botaoData = new Button("Cálculo de Datas");

        btnAdd.setFont(Font.font(17));
        btnAdd.setPrefSize(150,50);
        btnSub.setFont(Font.font(17));
        btnSub.setPrefSize(150,50);
        btnDif.setFont(Font.font(13));
        lblOutDia.setFont(Font.font(12));
        btnDif.setPrefSize(300,50);
        lbldiferencaData.setFont(Font.font(17));
        lblInputDia.setFont(Font.font(13));
        txtNum.setFont(Font.font(13));
        txtNum.setMaxWidth(50);

        InDatePicker = new DatePicker();
        InDatePicker.setValue(LocalDate.now());
        OutDatePicker = blockDatePicker(InDatePicker);
        OutDatePicker.setValue(LocalDate.now().plusDays(1));

        Label label = new Label("Calculadora Datas:");
        label.setFont(Font.font(20));

        GridPane gridPane = new GridPane();
        gridPane.setHgap(5);
        gridPane.setVgap(5);

        Label Inlabel = new Label("De:");
        gridPane.add(Inlabel, 0, 0);
        GridPane.setHalignment(Inlabel, HPos.LEFT);
        gridPane.add(InDatePicker, 0, 1);

        Label Outlabel = new Label("Até:");
        gridPane.add(Outlabel, 1, 0);
        GridPane.setHalignment(Outlabel, HPos.LEFT);
        gridPane.add(OutDatePicker, 1, 1);

        /*gridPane.addRow(2, lblInputDia, txtNum);
        gridPane.add(lblOutDia,0,3);
        gridPane.addRow(4, btnAdd, btnSub);
        gridPane.addRow(5,btnDif, lbldiferencaData);*/

        HBox hBox = new HBox();
        hBox.setAlignment(Pos.CENTER_LEFT);
        hBox.setSpacing(5);
        hBox.getChildren().addAll(lblInputDia, txtNum);

        HBox hBox2 = new HBox();
        hBox2.setAlignment(Pos.CENTER);
        hBox2.getChildren().addAll(lblOutDia);

        HBox hBoxButtons = new HBox();
        hBoxButtons.setSpacing(10);
        hBoxButtons.getChildren().addAll(btnAdd,btnSub);

        VBox vBox = new VBox(label, gridPane, hBox, hBox2, hBoxButtons, btnDif, lbldiferencaData);
        vBox.setPadding(new Insets(25));
        vBox.setSpacing(10);

        this.setBackground(new Background(new BackgroundFill(Color.color(1,1,1), CornerRadii.EMPTY, Insets.EMPTY)));
        this.setCenter(vBox);
    }
    public void registerHandlers(){
        Main.opcao.addListener(observable -> Platform.runLater(this::update2));

        btnDif.setOnAction(e -> {
            Main.calculadoraManager.diferencaDaData(InDatePicker.getValue(),OutDatePicker.getValue());
            lbldiferencaData.setText("Diferença de " + Main.calculadoraManager.toStringDiferenceForDates("Y") + " ano(s) e " +
                    Main.calculadoraManager.toStringDiferenceForDates("D") + " dia(s)");
        });
        btnAdd.setOnAction(e -> lblOutDia.setText("Data: " +
                Main.calculadoraManager.DateDia(txtNum.getText(),InDatePicker.getValue(),true)));
        btnSub.setOnAction(e -> lblOutDia.setText("Data: " +
                Main.calculadoraManager.DateDia(txtNum.getText(),InDatePicker.getValue(),false)));
    }

    private DatePicker blockDatePicker(DatePicker InDatePicker) {
        DatePicker OutDatePicker = new DatePicker();
        final Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(InDatePicker.getValue().plusDays(1))) {
                            setDisable(true);
                            setStyle("-fx-background-color: #a4a4a4;");
                        }
                        long d = ChronoUnit.DAYS.between(InDatePicker.getValue(), item);
                        setTooltip(new Tooltip("Diferença de " + d + " dias"));
                    }
                };
            }
        };
        OutDatePicker.setDayCellFactory(dayCellFactory);
        return OutDatePicker;
    }
    public void update(){

    }

    private void update2() {
        this.setVisible(Main.opcao.getValue() == 2);
    }
}
/*
    private void DataScene() {
        final Stage d = new Stage();
        d.initModality(Modality.APPLICATION_MODAL);
        d.initOwner(this.getScene().getWindow());
        VBox vbox = new VBox(20);
        d.setTitle("Calcular Datas");

        Button btnAdd = new Button("Adicionar");
        Button btnSub = new Button("Subtrair");
        Button btnDif = new Button("Diferença entre datas");
        Label lbldiferencaData = new Label();
        Label lblInputDia = new Label("Dia:");
        TextField txtNum = new TextField();

        btnAdd.setFont(Font.font(17));
        btnAdd.setPrefSize(150,50);
        btnSub.setFont(Font.font(17));
        btnSub.setPrefSize(150,50);
        btnDif.setFont(Font.font(13));
        btnDif.setPrefSize(150,50);
        lbldiferencaData.setFont(Font.font(17));
        lblInputDia.setFont(Font.font(13));
        txtNum.setFont(Font.font(13));

        DatePicker InDatePicker = new DatePicker();
        InDatePicker.setValue(LocalDate.now());
        DatePicker OutDatePicker = blockDatePicker(InDatePicker);
        OutDatePicker.setValue(LocalDate.now().plusDays(1));

        GridPane gridPane = new GridPane();
        gridPane.setHgap(5);
        gridPane.setVgap(5);

        Label Inlabel = new Label("De:");
        gridPane.add(Inlabel, 0, 0);
        GridPane.setHalignment(Inlabel, HPos.LEFT);

        gridPane.add(InDatePicker, 0, 1);
        gridPane.add(txtNum,1,1);

        Label Outlabel = new Label("Até:");
        gridPane.add(Outlabel, 0, 2);
        GridPane.setHalignment(Outlabel, HPos.LEFT);

        gridPane.add(lblInputDia, 1, 2);
        gridPane.add(OutDatePicker, 0, 3);

        gridPane.addRow(4, btnAdd, btnSub);
        gridPane.addRow(5,btnDif, lbldiferencaData);
        vbox.setPadding(new Insets(10));
        vbox.getChildren().add(gridPane);

        Scene dScene = new Scene(vbox, 300, 350);
        d.setScene(dScene);
        d.show();

        btnDif.setOnAction(e -> {
            Main.calculadoraManager.diferencaDaData(InDatePicker.getValue(),OutDatePicker.getValue());
            String aux = Main.calculadoraManager.toStringDate();
            lbldiferencaData.setText(aux == null ? "Selecione a data" : Integer.parseInt(aux) > 1 ? aux + " dias" : aux + " dia");
        });
        btnAdd.setOnAction(e -> {

        });
    }
 */