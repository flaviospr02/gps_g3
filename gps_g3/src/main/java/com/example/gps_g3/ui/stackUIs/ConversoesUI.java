package com.example.gps_g3.ui.stackUIs;

import com.example.gps_g3.Main;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.util.HashMap;

public class ConversoesUI extends BorderPane {
    Button btnConvToBin, btnConvToHexa;
    TextField txtValue;
    Label lblResultado;

    public ConversoesUI(){
        createViews();
        registerHandlers();
        update();
    }

    public void createViews(){
        btnConvToBin = new Button("To Binary");
        btnConvToHexa = new Button("To Hexadecimal");
        txtValue = new TextField("");
        lblResultado = new Label();

        Label label = new Label("Conversões:");
        label.setFont(Font.font(20));

        VBox vBox = new VBox(label, txtValue, btnConvToBin, btnConvToHexa, lblResultado);
        vBox.setPadding (new Insets (25));
        vBox.setSpacing(20);
        this.setBackground(new Background(new BackgroundFill(Color.color(1,1,1), CornerRadii.EMPTY, Insets.EMPTY)));
        this.setCenter(vBox);
    }
    public void registerHandlers() {
        Main.opcao.addListener(observable -> Platform.runLater(this::update2));

        btnConvToHexa.setOnAction(e -> {
            try {
                Main.calculadoraManager.binToHex(Long.parseLong(txtValue.getText()));
                lblResultado.setText("Valor em hexadecimal: " + Main.calculadoraManager.getHexRes());

            } catch (NumberFormatException ex) {
                lblResultado.setText("Insira um valor binario válido!");
            }
        });

        btnConvToBin.setOnAction(e -> {
            // try {
            Main.calculadoraManager.hexToBin(txtValue.getText());
            lblResultado.setText("Valor binário: " + Main.calculadoraManager.getBinRes());

           /* } catch (String ex) {
                lblResultado.setText("Insira um valor Hexadecimal válido!");
            }*/
        });
    }

    public void update(){
    }

    private void update2() {
        this.setVisible(Main.opcao.getValue() == 3);
    }
}
