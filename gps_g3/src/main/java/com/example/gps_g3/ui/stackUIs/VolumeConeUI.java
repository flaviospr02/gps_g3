package com.example.gps_g3.ui.stackUIs;

import com.example.gps_g3.Main;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class VolumeConeUI extends BorderPane {
    TextField raio, altura;
    Button calcula;
    Label resultadoLabel;

    public VolumeConeUI(){
        createViews();
        registerHandlers();
        update();
    }
    public void createViews(){
        raio = new TextField("Raio");
        altura = new TextField("Altura");
        calcula = new Button("Calcular");
        resultadoLabel = new Label();

        Label label = new Label("Volume Cone:");
        label.setFont(Font.font(20));

        VBox vBox = new VBox(label, raio, altura, calcula, resultadoLabel);
        vBox.setPadding (new Insets (25));
        vBox.setSpacing(20);
        this.setBackground(new Background(new BackgroundFill(Color.color(1,1,1), CornerRadii.EMPTY, Insets.EMPTY)));
        this.setCenter(vBox);
    }
    public void registerHandlers(){
        Main.opcao.addListener(observable -> Platform.runLater(this::update2));

        calcula.setOnAction(e->{
            Main.calculadoraManager.calculaVolumeCone(raio.getText(),altura.getText());
            update();
            /*try {
                double raioValue = Double.parseDouble(raio.getText());
                double alturaValue = Double.parseDouble(altura.getText());

                // Fórmula do volume do cone: V = (1/3) * π * raio^2 * altura
                double volume = Main.calculadoraManager.getVolumeCone(raioValue,alturaValue);

                resultadoLabel.setText("Volume do Cone: " + volume);
            } catch (NumberFormatException ex) {
                resultadoLabel.setText("Por favor, insira números válidos para raio e altura.");
            }*/
        });
    }
    public void update(){
        resultadoLabel.setText(Main.calculadoraManager.getResultadoVolCone());
    }

    private void update2() {
        this.setVisible(Main.opcao.getValue() == 4);
    }
    /*
    private void ConeScene(){
        final Stage d = new Stage();
        d.initModality(Modality.APPLICATION_MODAL);
        d.initOwner(this.getScene().getWindow());
        VBox vbox = new VBox(20);
        d.setTitle("Volume Cone");
        TextField raio = new TextField("Raio");
        TextField altura = new TextField("Altura");
        Button Calcula = new Button("Calcular");
        Label resultadoLabel = new Label();
        vbox.getChildren().addAll(raio,altura,Calcula,resultadoLabel);

        Scene cone = new Scene(vbox, 300, 350);
        d.setScene(cone);
        d.show();
        Calcula.setOnAction(e->{
            try {
                double raioValue = Double.parseDouble(raio.getText());
                double alturaValue = Double.parseDouble(altura.getText());

                // Fórmula do volume do cone: V = (1/3) * π * raio^2 * altura
                double volume = (1.0 / 3.0) * Math.PI * Math.pow(raioValue, 2) * alturaValue;

                resultadoLabel.setText("Volume do Cone: " + volume);
            } catch (NumberFormatException ex) {
                resultadoLabel.setText("Por favor, insira números válidos para raio e altura.");
            }
        });
    }*/
}
