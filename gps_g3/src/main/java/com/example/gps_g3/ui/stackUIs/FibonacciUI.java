package com.example.gps_g3.ui.stackUIs;

import com.example.gps_g3.Main;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class FibonacciUI extends BorderPane {
    ComboBox opcoes;
    Label resultado;
    TextField nInferior, nSuperior;
    Button calcular;

    public FibonacciUI(){
        createViews();
        registerHandlers();
        update();
    }
    public void createViews(){
        Label label = new Label("Calculadora Fibonacci:");
        label.setFont(Font.font(20));

        nInferior = new TextField();
        nSuperior = new TextField();
        nInferior.setMaxWidth(50);
        nSuperior.setMaxWidth(50);

        HBox hbox = new HBox(new Label("Fibonacci de n para n ="), nInferior, new Label(" até "), nSuperior);

        resultado = new Label();
        resultado.setStyle("-fx-border-color: black; -fx-border-width: 1; -fx-background-color: white; -fx-min-width: 250; -fx-min-height: 30; -fx-text-alignment: center");

        calcular = new Button("Calcular");

        VBox vBox = new VBox(label, hbox, calcular, resultado);
        vBox.setPadding (new Insets (25));
        vBox.setSpacing(20);

        this.setBackground(new Background(new BackgroundFill(Color.color(1,1,1), CornerRadii.EMPTY, Insets.EMPTY)));
        this.setCenter(vBox);
    }
    public void registerHandlers(){
        Main.opcao.addListener(observable -> Platform.runLater(this::update2));
        calcular.setOnAction( event -> {
            Main.calculadoraManager.calcularFibonacci(nInferior.getText(), nSuperior.getText());
            update();
        });
    }
    public void update(){
        resultado.setText(Main.calculadoraManager.getResultadoFibonacci());
    }

    private void update2() {
        this.setVisible(Main.opcao.getValue() == 6);
    }
}
