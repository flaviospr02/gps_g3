package com.example.gps_g3.ui.stackUIs;

import com.example.gps_g3.Main;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class FatorialUI extends BorderPane {
    TextField numero;
    Button calcula;
    Label resultadoLabel;
    public FatorialUI(){
        createViews();
        registerHandlers();
        update();
    }
    public void createViews() {
        numero = new TextField("Numero");
        calcula = new Button("Calcular");
        resultadoLabel = new Label();

        Label label = new Label("Calculadora Fatorial:");
        label.setFont(Font.font(20));

        VBox vBox = new VBox(label, numero, calcula, resultadoLabel);
        vBox.setPadding (new Insets (25));
        vBox.setSpacing(20);

        this.setBackground(new Background(new BackgroundFill(Color.color(1,1,1), CornerRadii.EMPTY, Insets.EMPTY)));
        this.setCenter(vBox);
    }
    public void registerHandlers(){
        Main.opcao.addListener(observable -> Platform.runLater(this::update2));

        calcula.setOnAction(e->{
            Main.calculadoraManager.calcularFatorial(numero.getText());
            update();
        });
    }

    public void update(){
        resultadoLabel.setText(Main.calculadoraManager.getResultadoFAtorial());
    }

    private void update2() {
        this.setVisible(Main.opcao.getValue() == 5);
    }
}
