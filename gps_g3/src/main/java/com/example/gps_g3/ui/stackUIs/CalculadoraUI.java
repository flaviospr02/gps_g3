package com.example.gps_g3.ui.stackUIs;

import com.example.gps_g3.Main;
import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.util.ArrayList;

public class CalculadoraUI extends BorderPane {

    private Label label;
    private ArrayList<Button> botoes;

    public CalculadoraUI(){
        createViews();
        registerHandler();
        update();
    }

    private void createViews() {
        label = new Label();
        botoes = new ArrayList<>();

        label.setFont(Font.font(25));
        label.setPrefHeight(50);
        label.setPrefWidth(220);
        label.setAlignment(Pos.TOP_RIGHT);
        label.setStyle("-fx-background-color: #dcecd2; -fx-border-weight: 2; -fx-border-color: black");

        TilePane tile = new TilePane();
        tile.setHgap(5);
        tile.setVgap(5);
        tile.setAlignment(Pos.CENTER);

        for(int i = 0; i < 10; i++)
            botoes.add(new Button(Integer.toString(i)));

        tile.getChildren().addAll(botoes);

        TilePane tile2 = new TilePane();
        tile2.setHgap(5);
        tile2.setVgap(5);
        tile2.setAlignment(Pos.BOTTOM_CENTER);

        Separator sep = new Separator();
        sep.setHalignment(HPos.CENTER);
        sep.setMaxWidth(getMaxWidth());
        Separator sep2 = new Separator();

        botoes.add(new Button("/"));
        botoes.add(new Button("*"));
        botoes.add(new Button("-"));
        botoes.add(new Button("+"));
        botoes.add(new Button("^"));
        botoes.add(new Button("+/-"));
        botoes.add(new Button("C"));
        botoes.add(new Button("CE"));
        botoes.add(new Button("√"));
        botoes.add(new Button("="));

        for(int i = 10; i < 20; i++)
            tile2.getChildren().add(botoes.get(i));

        for(int i = 0; i < 20; i++) {
            botoes.get(i).setFont(Font.font(17));
            botoes.get(i).setPrefSize(50,50);
        }
        VBox vBox = new VBox(label, tile, sep, tile2, sep2);
        vBox.setAlignment(Pos.CENTER);
        vBox.setPadding(new Insets(10));
        vBox.setSpacing(10);
        this.setCenter(vBox);
    }

    private void registerHandler() {
        Main.opcao.addListener(observable -> Platform.runLater(this::update2));

//------OPERANDOS
        botoes.get(0).setOnAction( event -> {
            Main.calculadoraManager.seleciona(0);
            update();
        });
        botoes.get(1).setOnAction( event -> {
            Main.calculadoraManager.seleciona(1);
            update();
        });
        botoes.get(2).setOnAction( event -> {
            Main.calculadoraManager.seleciona(2);
            update();
        });
        botoes.get(3).setOnAction( event -> {
            Main.calculadoraManager.seleciona(3);
            update();
        });
        botoes.get(4).setOnAction( event -> {
            Main.calculadoraManager.seleciona(4);
            update();
        });
        botoes.get(5).setOnAction( event -> {
            Main.calculadoraManager.seleciona(5);
            update();
        });
        botoes.get(6).setOnAction( event -> {
            Main.calculadoraManager.seleciona(6);
            update();
        });
        botoes.get(7).setOnAction( event -> {
            Main.calculadoraManager.seleciona(7);
            update();
        });
        botoes.get(8).setOnAction( event -> {
            Main.calculadoraManager.seleciona(8);
            update();
        });
        botoes.get(9).setOnAction( event -> {
            Main.calculadoraManager.seleciona(9);
            update();
        });
//------OPERADORES

        botoes.get(10).setOnAction( event -> {
            Main.calculadoraManager.divisao();
            update();
        });
        botoes.get(11).setOnAction( event -> {
            Main.calculadoraManager.multiplicacao();
            update();
        });
        botoes.get(12).setOnAction( event -> {
            Main.calculadoraManager.subtracao();
            update();
        });
        botoes.get(13).setOnAction( event -> {
            Main.calculadoraManager.soma();
            update();
        });
        botoes.get(14).setOnAction( event -> {
            Main.calculadoraManager.potencia();
            update();
        });
        botoes.get(15).setOnAction( event ->{
            Main.calculadoraManager.invsinal();
            update();
        });
        botoes.get(16).setOnAction( event -> {
            Main.calculadoraManager.limpar();
            update();
        });
        botoes.get(17).setOnAction( event -> {
            Main.calculadoraManager.descalcula();
            update();
        });
        botoes.get(18).setOnAction( event -> {
            Main.calculadoraManager.raizquad();
            update();
        });
        botoes.get(19).setOnAction( event -> {
            Main.calculadoraManager.fimLinhaCalculo();
            update();
        });
    }

    private void update() {
        label.setText(Main.calculadoraManager.toString());
    }

    private void update2() {
        this.setVisible(Main.opcao.getValue() == 1);
    }
}

