package com.example.gps_g3;

import com.example.gps_g3.model.CalculadoraManager;
import com.example.gps_g3.ui.MyCalculator;
import javafx.application.Application;
import javafx.beans.property.SimpleIntegerProperty;

public class Main {
    public static CalculadoraManager calculadoraManager;
    public static SimpleIntegerProperty opcao = new SimpleIntegerProperty(1);
    // 1 - calculadora;
    // 2 - datas;
    // 3 - conversor;
    // 4 - Volume Cone;
    // 5 - factorial;
    // 6 - fibonacci
    // 7 - Volume Cilindro
    static {
        calculadoraManager = new CalculadoraManager();
    }
    public static void main(String[] args) {
    Application.launch(MyCalculator.class,args);
}
}
