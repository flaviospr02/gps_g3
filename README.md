# Project Title
Digital & Mechanics

## Contents
- [Team](#team)
- [Vision and Scope](#vision-and-scope)
- [Requirements](#requirements)
    - [Use case diagram](#use-case-diagram)
    - [User stories and prototypes](#user-stories-and-prototypes)
- [Architecture and Design](#architecture-and-design)
    - [Domain Model](#domain-model)
- [Implementation](#implementation)
    - [Product Increment 1](#product-increment-1)
    - [Product Increment 2](#product-increment-2)
    - [Product Increment 3](#product-increment-3)
    - [Product Increment 4](#product-increment-4)

## Team

- Joao Costa a2021141594
- Flávio Rodrigues a2020137206
- Isabel Prieto 2021133536
- Daniel Rodrigues a2021142013
- Francisco Mota a2014008556

***

## Vision and Scope

#### Problem Statement
##### Project background

Este projeto surgiu no âmbito de ultrapassar o problema que a falta de organização causa na pesquisa de informação sobre o histórico de peças e manutenção de um ou mais carros.  
Efetivamente, um condutor de um automóvel deve fazer a manutenção e inspeção do veículo com regularidade e muitas vezes a organização dos documentos ou datas respetiva a estas intervenções não são bem geridas. Tem-se como exemplo o seguinte: Muitas pessoas não apontam ou até esquecem de quando precisam de trocar o óleo do carro. É sabido que os óleos têm que ser mudados ao fim de X quilómetros realizados pelo automóvel e a perda dos registo impede ter-se a noção exata dos quilometros já realizados e isto pode acabar por danificar gravemente o veículo. Segue-se outro exemplo: Apontar quais peças já foram trocadas e o preço. Será útil no futuro saber os gastos naquele veículo e quais as peças que já foram arranjadas. Este problema prevalece até aos dias de hoje e pode acabar por ser dispendioso em alguns fatores quando se vai ao mecânico.
Dado este panorama, o projeto consiste num histórico digital relativo a um automóvel no qual se pode registar e aceder a conteúdo alterado, corrigido ou em falta.

##### Stakeholders

Proprietários de Veículos:

Os proprietários de veículos são as principais partes interessadas que utilizam a aplicação para armazenar e gerir de forma segura as informações do seu veículo digitalmente, procurando conveniência, organização e facilidade de acesso sem precisar de carregar documentos físicos.

##### Users

Proprietários de Veículos:
    Registrar e acessar o histórico de manutenção de seus veículos.
    Receber lembretes para a manutenção programada, como troca de óleo, inspeção, etc.
    Registrar informações sobre peças substituídas e custos associados.

Usuários do Site em Casa (Home Website Users):
    Acessar e atualizar o histórico de manutenção de seus veículos de forma fácil e intuitiva.
    Receber notificações e alertas relacionados à manutenção de seus veículos.

***

#### Vision & Scope of the Solution

##### Vision statement

Objetivo é ajudar a registar e a não perder o registo de manutenção e gastos das diversas peças e quais nos automóveis. Assim, pretende-se registar a data das manutenções do carro, tal como, troca de susbtâncias liquidas e peças comuns a desgastarem, tais como, óleo do motor, liquido da resfrigeração, oléo dos travões, baterias, travões, pneus, discos de travões, suspensão, filtros (ar e combustivel), rotolas, velas, lâmpadas, correia de distribuição, fusiveis etc.
A aplicação é capaz de avisar com antecedência várias vezes sobre o estado das peças que têm que ser trocadas dentro do prazo, com uma prioridade definida, tal como o óleo do carro e correia de distribuição. Paralelamente, é possivel aceder a estes registos e apontar as devidas notas e ainda fazer registos fotográficos para identificar melhor a peça a procurar (caso seja preciso mais tarde outra nova peça igual há já colocada anteriormente). Consequentemente, com os dados assim organizados é mais fácil comprar peças que se procura. Adicionalmente, na venda do automóvel estes dados podem ser partilhados na app, ficando assim o próximo condutor a saber do que o carro realmente tem ou levou ao longo da sua vida, dando uma imagem de valor e interesse pelo antigo dono do carro, causando assim até uma melhor e mais tranparente imagem ao negócio. 
Finalmente, como o objetivo desta aplicação é de funcionar como uma compilação de informação relativa a um veículo, é concebível que qualquer pessoa usufrua dela respeitando os direitos de privacidade.

##### List of features

    Registo de automóvel:
        - Permite registar um ou mais automóveis na plataforma.

    Registo de manutenção/revisão:
        - Permite registar as mudanças periódicas do automóvel.

    Registo de peças mudadas:
        - Permite registar, com devido nome, fotografia da peça reposta e o respetivo preço, valor pelo qual custou.

    Gráfico geral de gastos no carro:
        - Apresenta mensalmente e anualmente os gastos efetuados em manutenção e peças compradas para o respetivo automóvel.

    Notificações importantes:
        - Apresenta ao utilizador importantes informações, para evitar futuros problemas no seu veiculo.

    Exportar informações e partilhar informações para outros dispositivos:
        - Guarda na cloud esta informação(para não existir futuras perdas de dados);
        - Permite dar esta informação a outro utilizador, sendo novo dono, ou até a um familiar para ficar com este registo atualizado no seu dispositivo.

##### Features that will not be developed

Mecânicos/Oficinas:
    Acessar o histórico de manutenção dos veículos de seus clientes;
    Visualizar as últimas manutenções realizadas para melhor diagnóstico e serviço.

Representantes de Suporte (Support Reps):
    Auxiliar os usuários na navegação e utilização eficaz do sistema;
    Resolver problemas técnicos ou fornecer informações adicionais quando necessário.

Digitalização e Carregamento de Documentos:
    Integrar uma funcionalidade que permita aos utilizadores digitalizarem documentos físicos usando a câmara do seu dispositivo e      carregá-los diretamente na aplicação, convertendo-os para formato digital.

Categorização e Etiquetagem de Documentos:  
    Permitir que os utilizadores classifiquem e etiquetem os seus documentos com base no tipo de documento, data de validade ou outros critérios personalizados para uma gestão eficiente e recuperação fácil.

Capacidade de Partilha e Transferência:
    Permitir que os utilizadores partilhem documentos específicos de forma segura com partes autorizadas, como agentes de seguros, mecânicos ou outras partes interessadas, diretamente a partir da aplicação.

##### Risk

Dados falsos no momento de venda a mostrar ao cliente (possivel novo dono), caso este queira o registo e seguir por conta própria este registo já criado pelo atual dono.

##### Assumptions

Preparação do utilizador:
    Utilizadores estarem comfortaveis com a utilização de telemoveis e suas aplicações, permitindo uma boa adaptação com o gerenciamento das informações.

Compatibilidade com Dispositivos:
    A aplicação é compatível com uma ampla gama de smartphones e sistemas operativos para acomodar os diversos dispositivos utilizados pelos potenciais utilizadores.

Preocupações com a Privacidade dos Dados:
    Os utilizadores confiam que a aplicação irá empregar medidas de segurança robustas para proteger os seus dados sensíveis, abordando preocupações relacionadas com a privacidade dos dados e o acesso não autorizado.

***

## Requirements

#### Use Case Diagram

![Use case diagram](imgs/UML_use_case_example-800x707.png)

***

##### Use Case 1
- Actor: Actor x
- Description: Description of use case number 1
- Preconditions:
    - Precondition 1
    - Precondition 2
    - Without preconditions
- Postconditions:
    - Postcondition 1
    - Postcondition 2
    - Without postcondition
- Normal flow:
    - The user ...
    - The user ...
- Alternative flows:
    - The user ...
    - The user ...

***

##### Use Case 2

***

##### Use Case 3

***

#### User Stories and Prototypes

***

##### User Story 1

História de um condutor diário, que todos os dias faz imensos quilómetros com o seu carro. Trabalha na área da saúde até hoje e o tempo que lhe resta após o horário de trabalho é muito pouco. Continha sempre contas a pagar, e hás vezes existia algo que escapava a esta pessoa por comprar, ou arranjar, seja na casa, carro... Com isto descreveu a nós o seu problema, as datas e tempo a cumprir fora do trabalho. Aconteceu que há cerca de um ano existiu um pequeno incidente, que podia ter-se tornado num pesadelo a nivel económico e podia ser muito complicado arranjar uma alternativa rápida. O carro precisava de uma correia de distribuição nova. E o seu utilizador acabou por não se recordar de realizar a devida mudança, ter de ir ao mecânico por esta preciosa peça que tem que ser nova, pronta para mais quilómetros. No entanto, teve a sorte de esta aguentar até ao dia que foi mudada, foi por bastante pouco que ficava sem transporte diário. A sorte do nosso cliente é que calhou em conversa com uns colegas de trabalho, sobre mecânicos, e que quando lá chegou estava quase a partir. Apesar disto não se recordava do valor da antiga correia, e como não pediu orçamento no mecânico ainda teve uma surpresa indesejável com o preço da fatura.

###### Acceptance Criteria

Registar:

- Data de colocação da peça
- Data de aviso de remendar/trocar/reparar a peça
- Preço da peça colocada
- Histórico da mesma peça colocada, com preços e datas registadas
- Imagem da peça
- Oficina usada caso se aplique

###### Prototype

A prototype of user story 1 should be here. You can see in (#use-case-diagram) how to import an image.

***

##### User Story 2

História de um recém cartado, que 1 semana após adquirir o seu primeiro carro acabou por ter o seu motor sobreaquecido. O motivo que levou esta avaria foi a falta de óleo. O antigo dono não lhe informou da última vez que o óleo teria sido mudado, nem quando trocar o óleo. Para alem disso também não sabia o tipo de óleo nem tinha registos da sua manutenção.

Registar:

- Data da troca do óleo
- Data de aviso da próxima vez para trocar o óleo
- Preço da troca do óleo
- Histórico da troca de óleo, com preços e datas registadas
- Tipo de óleo
- Registo
- Oficina usada caso se aplique

##### User Story 3

***

## Architecture and Design

#### Domain Model

A domain model should be here. You can see in (#use-case-diagram) how to import an image.

***

## Implementation

#### Product Increment 1

##### Sprint Goal
The sprint goal was ...

##### Planned vs Implemented
For this iteration we planned to implement the:
- Feature 1
- Feature 2

For this iteration we implemented the:
- Feature 1
- Feature 2

##### Sprint Retrospective
- What we did well:
    - A
- What we did less well:
    - B
- How to improve to the next sprint:
    - C

***

#### Product Increment 2

***

#### Product Increment 3

***

#### Product Increment 4

***
